import os
import flask
from flask import request,Flask, redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging


app = Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
def todo():
    return render_template('calc.html')

@app.route("/_new", methods=['GET', 'POST'])
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.form['km']
    totalDist = request.form['distance']
    startTime = request.form['begin_time']
    startDate = request.form['begin_date']
    totalDist = float(totalDist)
    try:
        km = float(km)
    except:
          flask.flash("error: enter a valid distance".format())
          return redirect('/') 
    if km/totalDist >= 1.2:
        flask.flash("error: your brevet distance is more than 20 percent longer than the total race distance".format())
        return redirect('/') 
    if km < 0:
        flask.flash("error: you entered a negative distance".format())
        return redirect('/') 
    

    brevet_start_time = arrow.get(startDate + 'T' + startTime).format()
    
    app.logger.debug("km= {}".format(km))
    app.logger.debug("totalDist= {}".format(totalDist))
    app.logger.debug("start date= {}".format(startDate))
    app.logger.debug("start time= {}".format(startTime))
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(totalDist, km, brevet_start_time)
    close_time = acp_times.close_time(totalDist, km, brevet_start_time)
    item_doc = {
        'distance': km,
        'opentime': open_time,
        'closetime': close_time,
        'location': request.form['location']
    }
    db.tododb.insert_one(item_doc)
    
    return redirect('/') 

@app.route('/display', methods=['GET', 'POST'])
def display():
    if db.tododb.count({}) != 0:
        _items = db.tododb.find().sort( [('distance', 1)] )
        items = [item for item in _items]
        return render_template('display.html', items=items)
    else:
        flask.flash("error: you haven't entered any times".format())
        return redirect('/')

@app.route('/reset', methods=['GET', 'POST'])
def reset():
    db.tododb.drop()
    return redirect('/')

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
